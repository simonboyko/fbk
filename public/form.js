function checkValidity(e) {

  function checkName(field) {
    var fieldWrp = field.parentElement;

    if (field.validity.valueMissing) {
      setInvalidClass(field);
      createTooltip(fieldWrp,'Имя обязательно для заполнения. Анонимам нельзя!');
      stopSubmit = true;
    } else {
      removeInvalidClass(field);
      removeTooltip(fieldWrp);
    }
  }

  function checkPhone(field) {
    var fieldWrp = field.parentElement;

    if (field.validity.patternMismatch) {
      setInvalidClass(field);
      createTooltip(fieldWrp,'Телефон введен неправильно');
      stopSubmit = true;
    } else {
      removeInvalidClass(field);
      removeTooltip(fieldWrp);
    }
  }

  function checkEmail(field) {
    var fieldWrp = field.parentElement;

    if (field.validity.valueMissing) {
      setInvalidClass(field);
      createTooltip(fieldWrp,'Вам обязательно нужно заполнить поле email');
      stopSubmit = true;
    } else if (field.validity.typeMismatch) {
      setInvalidClass(field);
      createTooltip(fieldWrp,'Ой... Email введен неправильно((');
      stopSubmit = true;
    } else {
      removeInvalidClass(field);
      removeTooltip(fieldWrp);
    }
  }

  function checkFz152(field) {
    var fieldWrp = field.parentElement;

    if (field.validity.valueMissing) {
      setInvalidClass(field);
      createTooltip(fieldWrp,'Вы должны принять договор-оферту, чтобы отправить форму');
      stopSubmit = true;
    } else {
      removeInvalidClass(field);
      removeTooltip(fieldWrp);
    }
  }


  var stopSubmit = false;
  // var form = document.querySelector('.form');
  var email = document.querySelector('input[name="email"]');
  var name = document.querySelector('input[name="first-name"]');
  var fz152 = document.querySelector('input[name="fz152"]');
  var phone = document.querySelector('input[name="phone"]');

  checkName(name);
  checkEmail(email);
  checkFz152(fz152);
  checkPhone(phone);

  if (stopSubmit) e.preventDefault();
}

function setInvalidClass(el) {
  el.classList.add('-invalid');
}

function removeInvalidClass(el) {
  el.classList.remove('-invalid');
}

function createTooltip(parentEl, text) {
  var tooltip;

  if (!parentEl.querySelector('.form__tooltip')) {
    tooltip = document.createElement('div');

    tooltip.classList.add('form__tooltip');
    tooltip.innerHTML = text;

    parentEl.appendChild(tooltip);
  } else {
    tooltip = parentEl.querySelector('.form__tooltip');
    tooltip.innerHTML = text;
  }
}

function removeTooltip(parentEl) {
  var tooltip = parentEl.querySelector('.form__tooltip');

  if (tooltip) {
    parentEl.querySelector('.form__tooltip').remove();
  }
}


document.addEventListener('DOMContentLoaded', function () {
  'use strict';

  var button = document.querySelector('.form__submit');

  button.addEventListener('click', checkValidity);

});

// отправка формы
document.addEventListener('DOMContentLoaded', function () {
  'use strict';

  var form = document.querySelector('.form');

  function sendForm(action, method) {
    var xhr = new XMLHttpRequest();
    var FD = new FormData(form);

    if (!xhr) {
      alert('Giving up :( Cannot create an XMLHTTP instance');
      return false;
    }

    xhr.addEventListener('load', function(e) {
      var formWrp = form.parentElement;
      var answer = document.createElement('div');
      answer.classList.add('answer');
      answer.innerHTML = '<h2 class="answer__title">Вот данные, которые вы прислали</h2>\n'+
        e.target.responseText;

      // form.remove();
      form.classList.add('-hidden');
      // setTimeout(function () {
      //   form.remove()
      // })
      formWrp.appendChild(answer);
    });

    xhr.addEventListener('error', function(e) {
      alert('Ошибка! Форма не отправилась.');
    });

    xhr.open(method, action);

    xhr.send(FD);

  }

  form.addEventListener('submit', function (e) {
    sendForm(this.getAttribute('action'), this.getAttribute('method'));

    e.preventDefault();
  });

});