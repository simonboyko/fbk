const express = require('express');
const multer= require('multer');
const app = express();
const upload = multer();


app.use(upload.array());
app.use(express.static(__dirname + '/public'));

app.get('/', (req, res) => res.sendfile(__dirname + '/index.html'));

app.get('/form', (req, res) => res.send('/form'));

app.post('/form', (req, res) => {
  res.status(200);

  for (let i in req.body) {
    res.write(i + ': ' + req.body[i] + '<br>');
  }
  res.send();
});

app.listen(8082, () => console.log('Example app listening on port 8082!'));
